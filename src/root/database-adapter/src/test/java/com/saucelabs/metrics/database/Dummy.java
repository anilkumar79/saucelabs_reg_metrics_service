package com.saucelabs.metrics.database;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.time.Instant;

import com.saucelabs.metrics.objectmodel.BitbucketURLImpl;
import com.saucelabs.metrics.objectmodel.EmailId;
import com.saucelabs.metrics.objectmodel.EmailIdImpl;
import com.saucelabs.metrics.objectmodel.JenkinsURLImpl;
import com.saucelabs.metrics.objectmodel.Password;
import com.saucelabs.metrics.objectmodel.PasswordImpl;
import com.saucelabs.metrics.objectmodel.RegistrationDateImpl;
import com.saucelabs.metrics.objectmodel.RequestorNameImpl;
import com.saucelabs.metrics.objectmodel.TeamNameImpl;
import com.saucelabs.metrics.objectmodel.TestingTypeImpl;
import com.saucelabs.metrics.objectmodel.UserRegistrationDto;
import com.saucelabs.metrics.objectmodel.UserRegistrationDtoImpl;
import com.saucelabs.metrics.objectmodel.UserRegistrationIdImpl;
import com.saucelabs.metrics.objectmodel.Username;
import com.saucelabs.metrics.objectmodel.UsernameImpl;

public class Dummy {

	private Username username;
	
	private URI uri;
	
	private EmailId email;
	
	
	public Dummy(){
		this.email = new EmailIdImpl("dummy@test.com");
		this.userRegistrationDto = 
				new UserRegistrationDtoImpl(
						new UserRegistrationIdImpl(123L),						 
						new RequestorNameImpl("dummyRequestorName"), 
						new TeamNameImpl("dummyTeamName"), 
						new EmailIdImpl("dummy@test.com"), 
						new RegistrationDateImpl(Timestamp.from(Instant.now())), 
						new TestingTypeImpl("dummyTestingType"), 
						new BitbucketURLImpl("dummyBitbucketURl"), 
						new JenkinsURLImpl("jenkinsURL")
						);
		
		this.username = new UsernameImpl("root");
		this.password = new PasswordImpl("root");
		try {
			this.uri = new URI("uri");
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
	}
	
	public EmailId getEmail() {
		return email;
	}

	public void setEmail(EmailId email) {
		this.email = email;
	}

	private UserRegistrationDto userRegistrationDto;
	
	public URI getUri() {
		return uri;
	}

	public void setUri(URI uri) {
		this.uri = uri;
	}

	public UserRegistrationDto getUserRegistrationDto() {
		return userRegistrationDto;
	}

	public void setUserRegistrationDto(UserRegistrationDto userRegistrationDto) {
		this.userRegistrationDto = userRegistrationDto;
	}

	public Username getUsername() {
		return username;
	}

	public void setUsername(Username username) {
		this.username = username;
	}

	public Password getPassword() {
		return password;
	}

	public void setPassword(Password password) {
		this.password = password;
	}

	private Password password;
}
