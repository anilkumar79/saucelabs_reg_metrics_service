package com.saucelabs.metrics.database;


import static org.assertj.core.api.StrictAssertions.assertThat;

import java.util.Collection;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.saucelabs.metrics.core.DatabaseAdapter;
import com.saucelabs.metrics.objectmodel.UserRegistrationId;
import com.saucelabs.metrics.objectmodel.UsersView;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DatabaseAdapterImplIT {

    /*
    fields
     */
    private final Config config =
    		new ConfigFactory()
    		.construct();

    private final Dummy dummy =
    		new Dummy();

    /*
    test methods
     */
    @Test
    public void test1AddUsers_whenUserRegistrationDto_shouldReturnsRegistrationId(
    ) throws Exception {

        DatabaseAdapter objectUnderTest =
                new DatabaseAdapterImpl(
                        config.getDatabaseAdapterConfig()
                );

        UserRegistrationId
        		registrationId =
                				objectUnderTest
                				.addUsers(
                						dummy.getUserRegistrationDto()
                						);
        	
        assertThat(registrationId).isNotNull();
    }

    @Test
    public void testListUsers_whenEmailId_shouldReturnsListOfUsers(
    ) throws Exception {

        DatabaseAdapter objectUnderTest =
                new DatabaseAdapterImpl(
                        config.getDatabaseAdapterConfig()
                );

        Collection<UsersView>
                listEntitlements =
                			objectUnderTest
                			.getUsersWithIds(
                					dummy
                						.getEmail()
                					);

        assertThat(listEntitlements.size())
                .isGreaterThan(0);
    }

}
