package com.saucelabs.metrics.database;

public final class ConfigFactory {
	
	public Config construct(){
		
		return
				new Config(
					new DatabaseAdapterConfigFactoryImpl()
														 .construct()
				);
	}

}
