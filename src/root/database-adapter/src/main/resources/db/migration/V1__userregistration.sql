CREATE TABLE `userregistration` (
  `userId` BIGINT NOT NULL AUTO_INCREMENT,
  `requestorName` VARCHAR(100) NULL,
  `teamName` VARCHAR(100) NULL,
  `emailId` VARCHAR(100) NULL,
  `testingType` VARCHAR(100) NULL,
  `registrationDate` datetime NOT NULL,
  `bitbucketURL` varchar(100) DEFAULT NULL,
  `jenkinsURL` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`userId`))
;
