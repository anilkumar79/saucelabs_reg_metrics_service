package com.saucelabs.metrics.database;


import java.util.Collection;
import java.util.Date;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.saucelabs.metrics.objectmodel.EmailId;
import com.saucelabs.metrics.objectmodel.UsersView;


public final class InstrumentedListUserRegistrationsWithIdsFeatureImpl implements ListUserRegistrationsWithIdsFeature {

	private static final Logger LOGGER = LoggerFactory.getLogger(InstrumentedListUserRegistrationsWithIdsFeatureImpl.class);

	private final ListUserRegistrationsWithIdsFeature underlyingFeature;

	@Inject
	public InstrumentedListUserRegistrationsWithIdsFeatureImpl(@NonNull @UnderlyingFeature final ListUserRegistrationsWithIdsFeature underlyingFeature) {
    	this.underlyingFeature =
                        underlyingFeature;

	}

	@Override
	public Collection<UsersView> getUsersWithIds(
			@NonNull EmailId emailId) {

		long startTime = System.currentTimeMillis();

		Collection<UsersView> result = underlyingFeature
				.getUsersWithIds(emailId);

		long endTime = System.currentTimeMillis();

		LOGGER.debug(String
				.format("[In database getUsersWithIds call took %s millis.[start=%s, end=%s]",
						(endTime - startTime), new Date(startTime), new Date(
								endTime)));

		return result;

	}

}
