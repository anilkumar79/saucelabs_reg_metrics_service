package com.saucelabs.metrics.database;

import com.saucelabs.metrics.objectmodel.UserRegistrationDto;
import com.saucelabs.metrics.objectmodel.UserRegistrationId;

public interface AddUserRegistrationFeature {

	public UserRegistrationId addUsers(
    		UserRegistrationDto userRegistrationDtoList
    	    );

}
