package com.saucelabs.metrics.database;

import java.util.Collection;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.flywaydb.core.Flyway;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.saucelabs.metrics.core.DatabaseAdapter;
import com.saucelabs.metrics.objectmodel.EmailId;
import com.saucelabs.metrics.objectmodel.UserRegistrationDto;
import com.saucelabs.metrics.objectmodel.UserRegistrationId;
import com.saucelabs.metrics.objectmodel.UsersView;

public class DatabaseAdapterImpl 
		implements DatabaseAdapter {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    @Inject
    public DatabaseAdapterImpl(
            @NonNull DatabaseAdapterConfig config
    ) {

        // ensure database schema up to date
        Flyway flyway = new Flyway();
        flyway.setDataSource(
                config.getUri().toString(),
                config.getUsername().getValue(),
                config.getPassword().getValue());
        flyway.migrate();


        GuiceModule guiceModule =
                new GuiceModule(
                        config
                );

        injector =
                Guice.createInjector(guiceModule);

    }

    @Override
    public UserRegistrationId addUsers(
    		UserRegistrationDto userRegistrationDto
    	    ) {

       return  injector
                .getInstance(AddUserRegistrationFeature.class)
                .addUsers(
                		userRegistrationDto
                		);

    }

   	@Override
	public Collection<UsersView> getUsersWithIds(
			EmailId emailId
			) {
		return  injector
                .getInstance(ListUserRegistrationsWithIdsFeature.class)
                .getUsersWithIds(
                		emailId
                		);
	}

}
