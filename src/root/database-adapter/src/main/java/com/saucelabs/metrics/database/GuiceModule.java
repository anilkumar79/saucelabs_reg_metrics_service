package com.saucelabs.metrics.database;


import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

class GuiceModule
        extends AbstractModule {

    /*
    fields
     */
    private final DatabaseAdapterConfig config;

    /*
    constructors
     */
    public GuiceModule(
            @NonNull final DatabaseAdapterConfig config
    ) {

        this.config = config;

    }

    @Override
    protected void configure() {

        bindFactories();

        bindFeatures();

    }

    private void bindFactories() {

        bind(UserRegistrationRequestFactory.class)
        	.to(UserRegistrationRequestFactoryImpl.class);
        
        bind(UserRegistrationResponseFactory.class)
			.to(UserRegistrationResponseFactoryImpl.class);

    }

    private void bindFeatures() {

    	bind(ListUserRegistrationsWithIdsFeature.class)
				.to(ListUserRegistrationWithIdsFeatureImpl.class);

        bind(AddUserRegistrationFeature.class)
				.to(InstrumentedAddUserRegistrationFeatureImpl.class);

        bind(AddUserRegistrationFeature.class)
				.annotatedWith(UnderlyingFeature.class)
				.to(AddUserRegistrationFeatureImpl.class);

    }

    @Provides
    @Singleton
    SessionFactory sessionFactory() {

        Configuration configuration = new Configuration();
        configuration.addPackage(getClass().getPackage().getName());
        configuration.addAnnotatedClass(UserRegistration.class);
        configuration.configure();

        configuration.setProperty("hibernate.connection.url", config.getUri().toString());
        configuration.setProperty("hibernate.connection.username", config.getUsername().getValue());
        configuration.setProperty("hibernate.connection.password", config.getPassword().getValue());

        StandardServiceRegistryBuilder builder =
                new StandardServiceRegistryBuilder()
                        .applySettings(
                                configuration.getProperties());

        return configuration
                .buildSessionFactory(
                        builder.build());
    }
}
