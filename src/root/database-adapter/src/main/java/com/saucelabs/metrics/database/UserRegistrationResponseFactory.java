package com.saucelabs.metrics.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.saucelabs.metrics.objectmodel.UsersView;

public interface UserRegistrationResponseFactory {
	
	UsersView construct(
			@NonNull UserRegistration userRegistration
			);
	
}
