package com.saucelabs.metrics.database;


import java.util.Date;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.saucelabs.metrics.objectmodel.UserRegistrationDto;
import com.saucelabs.metrics.objectmodel.UserRegistrationId;

public class InstrumentedAddUserRegistrationFeatureImpl implements AddUserRegistrationFeature{

	private static final Logger LOGGER = LoggerFactory.getLogger(InstrumentedAddUserRegistrationFeatureImpl.class);

	private final AddUserRegistrationFeature underlyingFeature;

	@Inject
	public InstrumentedAddUserRegistrationFeatureImpl(@NonNull @UnderlyingFeature final AddUserRegistrationFeature underlyingFeature) {

		this.underlyingFeature =
                        underlyingFeature;

	}

	@Override
	public UserRegistrationId addUsers(
			UserRegistrationDto userRegistrationDtoList) {

		long startTime = System.currentTimeMillis();

		UserRegistrationId result = underlyingFeature
				.addUsers(userRegistrationDtoList);

		long endTime = System.currentTimeMillis();

		LOGGER.debug(String
				.format("[In database userRegistrationDtoList call took %s millis.[start=%s, end=%s]",
						(endTime - startTime), new Date(startTime), new Date(
								endTime)));

		return result;
	}

}
