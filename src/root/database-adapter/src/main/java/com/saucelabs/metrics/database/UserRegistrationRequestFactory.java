package com.saucelabs.metrics.database;

public interface UserRegistrationRequestFactory {

	UserRegistration construct(
    		  com.saucelabs.metrics.objectmodel.UserRegistrationDto userRegistrationDto
    );

}
