package com.saucelabs.metrics.database;

import javax.inject.Singleton;

import com.saucelabs.metrics.objectmodel.BitbucketURL;
import com.saucelabs.metrics.objectmodel.BitbucketURLImpl;
import com.saucelabs.metrics.objectmodel.EmailId;
import com.saucelabs.metrics.objectmodel.EmailIdImpl;
import com.saucelabs.metrics.objectmodel.JenkinsURL;
import com.saucelabs.metrics.objectmodel.JenkinsURLImpl;
import com.saucelabs.metrics.objectmodel.RequestorName;
import com.saucelabs.metrics.objectmodel.RequestorNameImpl;
import com.saucelabs.metrics.objectmodel.TeamName;
import com.saucelabs.metrics.objectmodel.TeamNameImpl;
import com.saucelabs.metrics.objectmodel.RegistrationDate;
import com.saucelabs.metrics.objectmodel.RegistrationDateImpl;
import com.saucelabs.metrics.objectmodel.TestingType;
import com.saucelabs.metrics.objectmodel.TestingTypeImpl;
import com.saucelabs.metrics.objectmodel.UserRegistrationId;
import com.saucelabs.metrics.objectmodel.UserRegistrationIdImpl;
import com.saucelabs.metrics.objectmodel.UsersView;
import com.saucelabs.metrics.objectmodel.UsersViewImpl;

@Singleton
public class UserRegistrationResponseFactoryImpl implements UserRegistrationResponseFactory {

	@Override
	public UsersView construct(
			UserRegistration userRegistration) {
		
		UserRegistrationId userRegistrationId = 
				new UserRegistrationIdImpl(userRegistration.getUserId());
		 
		RequestorName requestorName = 
				new RequestorNameImpl(userRegistration.getRequestorName());
		
		TeamName teamName = 
				new TeamNameImpl(userRegistration.getTeamName());
		
		EmailId emailId = 
				new EmailIdImpl(userRegistration.getEmailId());
		
		RegistrationDate registrationDate = 
				new RegistrationDateImpl(userRegistration.getRegistrationDate());
		
		TestingType testingType = 
				new TestingTypeImpl(userRegistration.getBitbucketURL());
		
		BitbucketURL bitbucketURL = 
				new BitbucketURLImpl(userRegistration.getBitbucketURL());
		
		JenkinsURL jenkinsURL = 
				new JenkinsURLImpl(userRegistration.getJenkinsURL());
		
		return
				new UsersViewImpl(
						  userRegistrationId,
						  requestorName,
						  teamName,
						  emailId,
						  registrationDate,
						  testingType,
						  bitbucketURL,
						  jenkinsURL
				);
	}

}
