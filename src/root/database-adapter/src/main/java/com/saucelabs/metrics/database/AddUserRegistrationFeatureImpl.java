package com.saucelabs.metrics.database;


import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.saucelabs.metrics.objectmodel.UserRegistrationDto;
import com.saucelabs.metrics.objectmodel.UserRegistrationId;
import com.saucelabs.metrics.objectmodel.UserRegistrationIdImpl;

@Singleton
public class AddUserRegistrationFeatureImpl 
		implements AddUserRegistrationFeature {
	
	
    private final SessionFactory sessionFactory;
    private final UserRegistrationRequestFactory userRegistrationRequestFactory;

    @Inject
    public AddUserRegistrationFeatureImpl(
            @NonNull final SessionFactory sessionFactory,
            @NonNull final UserRegistrationRequestFactory userRegistrationRequestFactory
    ) {

    	this.sessionFactory =
                         sessionFactory;


    	this.userRegistrationRequestFactory =
    			userRegistrationRequestFactory;

    }

    @Override
	public UserRegistrationId addUsers(
			UserRegistrationDto userRegistrationDto
			) {

		UserRegistrationId userRegistrationId;

		Session session = sessionFactory.openSession();

		Transaction tx = session.beginTransaction();

		try {
            		Long registrationId =  (Long) session.save(
            								userRegistrationRequestFactory.
            											construct(
            													userRegistrationDto
            											)
            										);

            userRegistrationId = 
            					new UserRegistrationIdImpl(registrationId);
            			
              	tx.commit();

        } catch(final Exception e) {

        	tx.rollback();
        	throw new RuntimeException("Exception while committing transaction:", e);

        } finally {

        	if (session != null) {
                session.close();
            }
        }
        return userRegistrationId;
	}


}
