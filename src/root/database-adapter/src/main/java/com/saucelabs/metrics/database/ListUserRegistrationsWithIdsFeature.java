package com.saucelabs.metrics.database;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.saucelabs.metrics.objectmodel.EmailId;
import com.saucelabs.metrics.objectmodel.UsersView;

interface ListUserRegistrationsWithIdsFeature {

	 public Collection<UsersView> getUsersWithIds(
	    		@NonNull EmailId emailId
	    		);
}
