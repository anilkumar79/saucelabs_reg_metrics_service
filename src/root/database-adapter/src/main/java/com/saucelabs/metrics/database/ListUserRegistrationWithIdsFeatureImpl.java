package com.saucelabs.metrics.database;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.saucelabs.metrics.objectmodel.EmailId;
import com.saucelabs.metrics.objectmodel.UsersView;

public class ListUserRegistrationWithIdsFeatureImpl 
	implements ListUserRegistrationsWithIdsFeature {
	
	private final SessionFactory sessionFactory;
    private final UserRegistrationResponseFactory userRegistrationResponseFactory;

    @Inject
    public ListUserRegistrationWithIdsFeatureImpl(
            @NonNull final SessionFactory sessionFactory,
            @NonNull final UserRegistrationResponseFactory userRegistrationResponseFactory
    ) {

    	this.sessionFactory =
                        sessionFactory;

    	this.userRegistrationResponseFactory =
    			userRegistrationResponseFactory;
    	

    }

    @SuppressWarnings("unchecked")
    @Override
	public Collection<UsersView> getUsersWithIds(
			@NonNull EmailId emailAddress
			) {
		 Session session = null;
	        try {
	        	
	        	/*List<Long> userRegistrationId = userRegistrationIds.stream()
	        											.map(id -> id.getValue())
	        											.collect(Collectors.toList());*/
	        	String emailId = emailAddress.getValue().trim();
	        	
	            session = sessionFactory.openSession();

	            List<UserRegistration> usersView =
	                    session
	                    	    .createQuery("from UserRegistration where emailId LIKE :emailId")	
	                    		.setParameter("emailId", emailId+"%")
	                    		.list();
	            return usersView
	                    .stream()
	                    .map(userRegistrationResponseFactory::construct)
	                    .collect(Collectors.toList());

	        } catch(final Exception e) {
	        	e.printStackTrace();
	        	throw new RuntimeException("exception while getting records:", e);
	        }
	        finally {
	        
	            if (session != null) {
	                session.close();
	            }

	        }
	}


}
