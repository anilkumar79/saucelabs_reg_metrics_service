package com.saucelabs.metrics.database;

import java.net.URI;
import java.net.URISyntaxException;

import com.saucelabs.metrics.objectmodel.Password;
import com.saucelabs.metrics.objectmodel.PasswordImpl;
import com.saucelabs.metrics.objectmodel.Username;
import com.saucelabs.metrics.objectmodel.UsernameImpl;


public class DatabaseAdapterConfigFactoryImpl
        implements DatabaseAdapterConfigFactory {

	@Override
	public DatabaseAdapterConfig construct() {

		return
				new DatabaseAdapterConfigImpl(
									constructURI(),
				                    constructUserName(),
				                    constructPassword()
		);
	}

	public URI constructURI() {

		try {

			String baseURI = System.getenv("DATABASE_URI");

			return
					new URI(
					  baseURI
					);

		} catch (URISyntaxException e) {

			throw new RuntimeException(e);

		}
	}

	public Username constructUserName(){

		return
				new UsernameImpl(
					   System.getenv("DATABASE_USERNAME")
				);

	}

	public Password constructPassword(){

		return
				new PasswordImpl(
					   System.getenv("DATABASE_PASSWORD")
				);

	}
}
