package com.saucelabs.metrics.database;

public interface DatabaseAdapterConfigFactory {

	DatabaseAdapterConfig construct();
}
