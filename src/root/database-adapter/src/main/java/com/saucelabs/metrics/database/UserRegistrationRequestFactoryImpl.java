package com.saucelabs.metrics.database;


import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.saucelabs.metrics.objectmodel.UserRegistrationDto;


@Singleton
public class UserRegistrationRequestFactoryImpl implements UserRegistrationRequestFactory{


	@Override
	public UserRegistration construct(
			@NonNull UserRegistrationDto userRegistrationDto) {

		UserRegistration entityObject = new UserRegistration();

		entityObject
			.setRequestorName(userRegistrationDto.getRequestorName().getValue());

		entityObject
			.setTeamName(userRegistrationDto.getTeamName().getValue());

		entityObject
			.setEmailId(
				userRegistrationDto
					.getEmailId()
					.getValue()
						);
		
		entityObject
		.setRegistrationDate(
				userRegistrationDto
					.getRegistrationDate()
					.getValue()
						);
		entityObject
			.setTestingType(userRegistrationDto.getTestingType().getValue());
		
		entityObject
			.setBitbucketURL(userRegistrationDto.getBitbucketURL().getValue());
		
		entityObject
			.setJenkinsURL(userRegistrationDto.getJenkinsURL().getValue());
	
		
        return
        		entityObject;
	}

}
