package com.saucelabs.metrics.database;



import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "userregistration")
class UserRegistration {

    /*
    fields
     */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long userId;

    private String requestorName;

    private String teamName;
    
    private String emailId;

    private String testingType;
    
    private Timestamp registrationDate;
    
    private String bitbucketURL;
    
    private String jenkinsURL;
    
	/*
    getter & setter methods
    */
    public Timestamp getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Timestamp registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getBitbucketURL() {
		return bitbucketURL;
	}

	public void setBitbucketURL(String bitbucketURL) {
		this.bitbucketURL = bitbucketURL;
	}

	public void setTestingType(String testingType) {
		this.testingType = testingType;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getTestingType() {
		return testingType;
	}
	
	public String getJenkinsURL() {
		return jenkinsURL;
	}

	public void setJenkinsURL(String jenkinsURL) {
		this.jenkinsURL = jenkinsURL;
	}

}
