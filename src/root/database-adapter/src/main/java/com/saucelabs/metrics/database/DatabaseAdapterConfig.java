package com.saucelabs.metrics.database;


import java.net.URI;

import com.saucelabs.metrics.objectmodel.Password;
import com.saucelabs.metrics.objectmodel.Username;

public interface DatabaseAdapterConfig {

    URI getUri();

    Username getUsername();

    Password getPassword();

}
