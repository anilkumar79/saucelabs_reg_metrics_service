package com.saucelabs.registrationservice.core;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.saucelabs.metrics.core.AddUserRegistrationFetaureImpl;
import com.saucelabs.metrics.core.DatabaseAdapter;
import com.saucelabs.metrics.objectmodel.UserRegistrationDto;
import com.saucelabs.metrics.objectmodel.UserRegistrationId;

@RunWith(MockitoJUnitRunner.class)
public class AddUserRegistrationFetaureTest {

	/*
    fields
     */
	@Mock
	private DatabaseAdapter databaseAdapter;
	
	private Dummy dummy = new Dummy();

	@InjectMocks
	private AddUserRegistrationFetaureImpl addUserRegistrationFetaureImpl;

	@Test
	public void test() {
		UserRegistrationId actual = dummy.getUsersDto().getUserRegistrationId();
		UserRegistrationDto dto = dummy.getUsersDto();
		when(databaseAdapter
				.addUsers(
						dto
						)
			).thenReturn(actual);

		UserRegistrationId expected = 
				addUserRegistrationFetaureImpl
					.addUsers(dto);
		
		assertThat(expected).isEqualTo(actual);
	}
}
