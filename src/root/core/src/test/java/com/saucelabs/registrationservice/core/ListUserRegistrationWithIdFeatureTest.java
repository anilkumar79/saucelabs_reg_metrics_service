package com.saucelabs.registrationservice.core;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.saucelabs.metrics.core.DatabaseAdapter;
import com.saucelabs.metrics.core.ListUserRegistrationsWithIdsFeatureImpl;
import com.saucelabs.metrics.objectmodel.UsersView;

@RunWith(MockitoJUnitRunner.class)
public class ListUserRegistrationWithIdFeatureTest {

	/*
    fields
     */
	@Mock
	private DatabaseAdapter databaseAdapter;
	
	private Dummy dummy = new Dummy();

	@InjectMocks
	private ListUserRegistrationsWithIdsFeatureImpl userListRegistrationWihtIdsFeatureImpl;

	@Test
	public void test() {

		when(databaseAdapter
				.getUsersWithIds(
						dummy.getEmailId()
						)
			)
			.thenReturn(
						dummy.getUsersView()
						);

		Collection<UsersView> usersViewList = 
				userListRegistrationWihtIdsFeatureImpl
						.getUsersWithEmailId(
								dummy.getEmailId()
						);
		assertThat(usersViewList.size())
        			.isEqualTo(1);
	}
}
