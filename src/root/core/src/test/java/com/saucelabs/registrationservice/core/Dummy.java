package com.saucelabs.registrationservice.core;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

import com.saucelabs.metrics.objectmodel.BitbucketURLImpl;
import com.saucelabs.metrics.objectmodel.EmailId;
import com.saucelabs.metrics.objectmodel.EmailIdImpl;
import com.saucelabs.metrics.objectmodel.JenkinsURLImpl;
import com.saucelabs.metrics.objectmodel.RequestorNameImpl;
import com.saucelabs.metrics.objectmodel.TeamNameImpl;
import com.saucelabs.metrics.objectmodel.RegistrationDateImpl;
import com.saucelabs.metrics.objectmodel.TestingTypeImpl;
import com.saucelabs.metrics.objectmodel.UserRegistrationDto;
import com.saucelabs.metrics.objectmodel.UserRegistrationDtoImpl;
import com.saucelabs.metrics.objectmodel.UserRegistrationIdImpl;
import com.saucelabs.metrics.objectmodel.UsersView;
import com.saucelabs.metrics.objectmodel.UsersViewImpl;

public class Dummy {

	private EmailId emailId = new EmailIdImpl("test@gmail.com");
	
	private Collection<UsersView> usersView = new ArrayList<UsersView>();
	
	
	public EmailId getEmailId() {
		return emailId;
	}

	public void setEmailId(EmailId emailId) {
		this.emailId = emailId;
	}

	public Collection<UsersView> getUsersView() {
		UsersView view =
				new UsersViewImpl(
						new UserRegistrationIdImpl(3L),
						new RequestorNameImpl("DummyRequestor"),
						new TeamNameImpl("DummyTeamName"),
						new EmailIdImpl("test@gmail.com"),
						new RegistrationDateImpl(Timestamp.valueOf(LocalDateTime.now())),
						new TestingTypeImpl(null),
						new BitbucketURLImpl("http://localhost:8000/bitbucket"),
						new JenkinsURLImpl("http://localhost:9000/jenkins")
				);
		usersView.add(view);
		return usersView;
	}
	
	public UserRegistrationDto getUsersDto() {
		UserRegistrationDto view =
				new UserRegistrationDtoImpl(
						new UserRegistrationIdImpl(3L),
						new RequestorNameImpl("DummyRequestor"),
						new TeamNameImpl("DummyTeamName"),
						new EmailIdImpl("test@gmail.com"),
						new RegistrationDateImpl(Timestamp.valueOf(LocalDateTime.now())),
						new TestingTypeImpl(null),
						new BitbucketURLImpl("http://localhost:8000/bitbucket"),
						new JenkinsURLImpl("http://localhost:9000/jenkins")
				);
		return view;
	}

	public void setUsersView(Collection<UsersView> usersView) {
		this.usersView = usersView;
	}
	
}
