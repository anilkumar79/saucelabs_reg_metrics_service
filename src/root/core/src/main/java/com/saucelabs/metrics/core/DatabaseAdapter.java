package com.saucelabs.metrics.core;

import java.util.Collection;

import com.saucelabs.metrics.objectmodel.EmailId;
import com.saucelabs.metrics.objectmodel.UserRegistrationDto;
import com.saucelabs.metrics.objectmodel.UserRegistrationId;
import com.saucelabs.metrics.objectmodel.UsersView;

public interface DatabaseAdapter {

	UserRegistrationId addUsers(
    		UserRegistrationDto userRegistrationDto
    	    ) ;

    Collection<UsersView> getUsersWithIds(
    		EmailId emailId
    		);
    
}
