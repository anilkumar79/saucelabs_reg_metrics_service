package com.saucelabs.metrics.core;


import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;

class GuiceModule extends
        AbstractModule {

    private final DatabaseAdapter databaseAdapter;

    public GuiceModule(
            @NonNull final DatabaseAdapter databaseAdapter
            ){
    	this.databaseAdapter =
                         databaseAdapter;

    }

    @Override
    protected void configure() {

        bindAdapters();
        bindFeatures();

    }

    private void bindAdapters() {

        bind(DatabaseAdapter.class)
                .toInstance(databaseAdapter);

    }

    private void bindFeatures() {

       bind(AddUserRegistrationFeature.class)
				.to(AddUserRegistrationFetaureImpl.class);
       
       bind(ListUserRegistrationsWithIdsFeature.class)
        		.to(ListUserRegistrationsWithIdsFeatureImpl.class);

    }
}
