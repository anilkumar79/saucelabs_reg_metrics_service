package com.saucelabs.metrics.core;

import java.util.Collection;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.saucelabs.metrics.objectmodel.EmailId;
import com.saucelabs.metrics.objectmodel.UserRegistrationDto;
import com.saucelabs.metrics.objectmodel.UserRegistrationId;
import com.saucelabs.metrics.objectmodel.UsersView;

public class CoreImpl 
		implements Core {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    @Inject
    public CoreImpl(
            @NonNull DatabaseAdapter databaseAdapter
    ) {

    	GuiceModule guiceModule =
                new GuiceModule(
                        databaseAdapter
                        );


        injector = Guice.createInjector(guiceModule);
    }

    @Override
	public UserRegistrationId addUsers(
			UserRegistrationDto userRegistrationDto
		    ) {

		 return
				 injector
				 	.getInstance(AddUserRegistrationFeature.class)
				 	.addUsers(
				 			userRegistrationDto
						 );
	}

    @Override
	public Collection<UsersView> getUsersWithIds(
			@NonNull EmailId emailId
			) {

		return
				injector
		         	.getInstance(ListUserRegistrationsWithIdsFeature.class)
		         	.getUsersWithEmailId(
		         			emailId
		         			);
	}


}
