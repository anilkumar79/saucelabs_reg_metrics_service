package com.saucelabs.metrics.core;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.saucelabs.metrics.objectmodel.EmailId;
import com.saucelabs.metrics.objectmodel.UsersView;

interface ListUserRegistrationsWithIdsFeature {

    Collection<UsersView> getUsersWithEmailId(
    						@NonNull EmailId emailId
    						);

}
