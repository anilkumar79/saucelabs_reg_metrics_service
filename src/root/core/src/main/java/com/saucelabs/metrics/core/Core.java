package com.saucelabs.metrics.core;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.saucelabs.metrics.objectmodel.EmailId;
import com.saucelabs.metrics.objectmodel.UserRegistrationDto;
import com.saucelabs.metrics.objectmodel.UserRegistrationId;
import com.saucelabs.metrics.objectmodel.UsersView;

public interface Core {

     UserRegistrationId addUsers(
    		UserRegistrationDto userRegistrationDtos
    	    );

     Collection<UsersView> getUsersWithIds(
    		@NonNull EmailId emailId
    		);

}
