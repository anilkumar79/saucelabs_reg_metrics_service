package com.saucelabs.metrics.core;


import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.saucelabs.metrics.objectmodel.EmailId;
import com.saucelabs.metrics.objectmodel.UsersView;

@Singleton
public class ListUserRegistrationsWithIdsFeatureImpl 
		implements ListUserRegistrationsWithIdsFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public ListUserRegistrationsWithIdsFeatureImpl(
            @NonNull final DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                         databaseAdapter;

    }

	@Override
	public Collection<UsersView> getUsersWithEmailId(
			@NonNull EmailId emailId
			) {

		return
                databaseAdapter
                	.getUsersWithIds(emailId);
	}

}
