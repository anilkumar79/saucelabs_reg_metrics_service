package com.saucelabs.metrics.core;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.saucelabs.metrics.objectmodel.UserRegistrationDto;
import com.saucelabs.metrics.objectmodel.UserRegistrationId;

public interface AddUserRegistrationFeature {

	UserRegistrationId addUsers(
			@NonNull UserRegistrationDto userRegistrationDtos
			);

}
