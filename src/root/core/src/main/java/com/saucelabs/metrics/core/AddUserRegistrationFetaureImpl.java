package com.saucelabs.metrics.core;


import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.saucelabs.metrics.objectmodel.UserRegistrationDto;
import com.saucelabs.metrics.objectmodel.UserRegistrationId;

@Singleton
public class AddUserRegistrationFetaureImpl 
		implements AddUserRegistrationFeature {
		
    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public AddUserRegistrationFetaureImpl(
            @NonNull final DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                         databaseAdapter;

    }

	@Override
	public UserRegistrationId addUsers(
			@NonNull UserRegistrationDto userRegistrationDtos
		    ) {
		UserRegistrationId userRegistrationId = null;
		try {
		userRegistrationId = 
				databaseAdapter
					.addUsers(
							userRegistrationDtos
						);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return userRegistrationId;
	}

}
