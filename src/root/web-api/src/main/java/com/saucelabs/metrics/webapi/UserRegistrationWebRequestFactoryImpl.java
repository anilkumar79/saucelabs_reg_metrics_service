package com.saucelabs.metrics.webapi;

import java.sql.Timestamp;

import org.springframework.stereotype.Component;

import com.saucelabs.metrics.objectmodel.BitbucketURL;
import com.saucelabs.metrics.objectmodel.BitbucketURLImpl;
import com.saucelabs.metrics.objectmodel.EmailId;
import com.saucelabs.metrics.objectmodel.EmailIdImpl;
import com.saucelabs.metrics.objectmodel.JenkinsURL;
import com.saucelabs.metrics.objectmodel.JenkinsURLImpl;
import com.saucelabs.metrics.objectmodel.RequestorName;
import com.saucelabs.metrics.objectmodel.RequestorNameImpl;
import com.saucelabs.metrics.objectmodel.TeamName;
import com.saucelabs.metrics.objectmodel.TeamNameImpl;
import com.saucelabs.metrics.objectmodel.RegistrationDate;
import com.saucelabs.metrics.objectmodel.RegistrationDateImpl;
import com.saucelabs.metrics.objectmodel.TestingType;
import com.saucelabs.metrics.objectmodel.TestingTypeImpl;
import com.saucelabs.metrics.objectmodel.UserRegistrationDtoImpl;
import com.saucelabs.metrics.objectmodel.UserRegistrationId;
import com.saucelabs.metrics.objectmodel.UserRegistrationIdImpl;

@Component
public class UserRegistrationWebRequestFactoryImpl 
		implements UserRegistrationWebRequestFactory{

	@Override
	public com.saucelabs.metrics.objectmodel.UserRegistrationDto construct(
			com.saucelabs.metrics.webapiobjectmodel.UsersWebDto usersWebDto) {

		UserRegistrationId userRegistrationId =
        		new UserRegistrationIdImpl(
        				usersWebDto
        				.getId()
        				);
		
        RequestorName requestorName =
        		new RequestorNameImpl(
        				usersWebDto
        				.getRequestorName()
        				);
        
        TeamName teamName =
        		new TeamNameImpl(
        				usersWebDto
        				.getTeamName()
        				);

        EmailId emailId =
        		new EmailIdImpl(
        				usersWebDto
        				.getEmail()
        				);

        java.util.Date currentDate = new java.util.Date();

		RegistrationDate registrationDate = 
				new RegistrationDateImpl(
						new Timestamp(currentDate.getTime())
					);

        TestingType testingType = new TestingTypeImpl(usersWebDto.getTestingType());
        
        BitbucketURL bitbucketURL =
        		new BitbucketURLImpl(
        				usersWebDto
        				.getBitbucketURL()
        				);
        
        JenkinsURL jenkinsURL =
        		new JenkinsURLImpl(
        				usersWebDto
        				.getJenkinsURL()
        				);
        
        return
                new UserRegistrationDtoImpl(
                		 userRegistrationId,
            			  requestorName,
            			  teamName,
            			  emailId,
            			  registrationDate,
            			  testingType,
            			  bitbucketURL,
            			  jenkinsURL
                	);
	}

}
