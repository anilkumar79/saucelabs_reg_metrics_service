package com.saucelabs.metrics.webapi;

import com.saucelabs.metrics.objectmodel.UsersView;
import com.saucelabs.metrics.webapiobjectmodel.UserRegistrationWebView;

public interface UserResgistrationViewResponseFactory {

	UserRegistrationWebView construct(
			UsersView usersView
			);
}
