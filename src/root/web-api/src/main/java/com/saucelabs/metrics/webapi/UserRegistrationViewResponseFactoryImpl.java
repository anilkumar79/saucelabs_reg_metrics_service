package com.saucelabs.metrics.webapi;

import java.text.SimpleDateFormat;

import org.springframework.stereotype.Component;

import com.saucelabs.metrics.objectmodel.UsersView;
import com.saucelabs.metrics.webapiobjectmodel.UserRegistrationWebView;

@Component
public class UserRegistrationViewResponseFactoryImpl 
		implements UserResgistrationViewResponseFactory {

	@Override
	public UserRegistrationWebView construct(UsersView usersView) {

		Long userRegistrationId =
				usersView
				.getUserRegistrationId().getValue();

		String requestorName =
				usersView
				 .getRequestorName()
				 .getValue();

		String teamName =
				usersView
				 .getTeamName()
				 .getValue();

		String email =
				usersView
				 .getEmailId()
				 .getValue();
		
		 String registrationDate = 
				 new SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
		 				.format(usersView.getRegistrationDate().getValue());

		 String testingType =
				 usersView
				 .getTestingType()
				 .getValue();
		 
		 String bitbucketURL =
					usersView
					 .getBitbucketURL()
					 .getValue();
			
		 String jenkinsURL =
					usersView
					 .getJenkinsURL()
					 .getValue();
			


		return new UserRegistrationWebView(
				userRegistrationId,
	    		requestorName,
	    		teamName,
	    		email,
	    		registrationDate,
	    		testingType,
	    		bitbucketURL,
	    		jenkinsURL
			);
	}

}
