package com.saucelabs.metrics.webapi;

import javax.inject.Singleton;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.saucelabs.metrics.api.UserRegistrationService;
import com.saucelabs.metrics.api.UserRegistrationServiceConfigFactoryImpl;
import com.saucelabs.metrics.api.UserRegistrationServiceImpl;


@Configuration
public class UserRegistrationServiceBean {

    @Bean
    @Singleton
    public UserRegistrationService userRegistrationService() {

        return
                new UserRegistrationServiceImpl(
                		new UserRegistrationServiceConfigFactoryImpl()
                			                                   .construct()
                );

    }
}
