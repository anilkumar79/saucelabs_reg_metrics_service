package com.saucelabs.metrics.webapi;

import com.saucelabs.metrics.objectmodel.UserRegistrationDto;

public interface UserRegistrationWebRequestFactory {
	
	UserRegistrationDto construct(
    		com.saucelabs.metrics.webapiobjectmodel.UsersWebDto usersWebDto
    );
    
}
