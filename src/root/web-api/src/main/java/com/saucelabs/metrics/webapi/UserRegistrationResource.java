package com.saucelabs.metrics.webapi;


import java.util.Collection;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.saucelabs.metrics.api.UserRegistrationService;
import com.saucelabs.metrics.objectmodel.EmailIdImpl;
import com.saucelabs.metrics.webapiobjectmodel.UserRegistrationWebView;
import com.saucelabs.metrics.webapiobjectmodel.UsersWebDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/user-registration")
@Api(value = "/user-registration", description = "Operations on user registration")
public class UserRegistrationResource {

    /*
    fields
     */
    private final UserRegistrationService userRegistrationService;

    private final UserRegistrationWebRequestFactory userRegistrationWebRequestFactory;

    private final UserResgistrationViewResponseFactory userResgistrationViewResponseFactory;

    

    @Inject
    public UserRegistrationResource(
            @NonNull final UserRegistrationService userRegistrationService,
            @NonNull final UserRegistrationWebRequestFactory userRegistrationWebRequestFactory,
            @NonNull final UserResgistrationViewResponseFactory userResgistrationViewResponseFactory
           
    ) {

    	this.userRegistrationService =
                        userRegistrationService;

    	this.userRegistrationWebRequestFactory =
                        userRegistrationWebRequestFactory;

    	this.userResgistrationViewResponseFactory =
                        userResgistrationViewResponseFactory;

    }

    @RequestMapping(
    		value = "/addUser",
    		method = RequestMethod.POST
    		)
    @ApiOperation(
    		value = "Register a user"
    		)
    public Long addUsers(
    		@RequestBody UsersWebDto users
    ){
    	Long userId =
    	userRegistrationService.
			addUsers(
					userRegistrationWebRequestFactory.construct(users)
			).getValue();
   		/*Collection<Long> userIds =
   				userRegistrationService.
   						addUsers(
   								userRegistrationWebRequestFactory.construct(users)
   						).stream()
   				         .map(registrationId -> registrationId.getValue())
   				         .collect(Collectors.toList());*/

   		return userId;

    }
    
    @RequestMapping(
            value = "emailId/{emailId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON
            )
    @ApiOperation(
            value = "gets the registration with the provided emailId"
            )
    public Collection<UserRegistrationWebView> getUsers(
    		@PathVariable("emailId") String emailId
    ) {

        /*List<UserRegistrationId> registrationId = registrationIds.stream()
        										   .map(id -> new UserRegistrationIdImpl(id))
        										   .collect(Collectors.toList());*/
        return
        		userRegistrationService
                        .getUsersWithEmailId(new EmailIdImpl(emailId))
                        .stream()
                        .map(userResgistrationViewResponseFactory::construct)
                        .collect(Collectors.toList());

    }

}
