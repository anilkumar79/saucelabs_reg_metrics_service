package com.saucelabs.metrics.webapi;

import static com.jayway.restassured.RestAssured.given;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jayway.restassured.RestAssured;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest({ "server.port=0" })
public class UserRegistrationResourceIT {

	/*
	 * fields
	 */
	private final Dummy dummy = new Dummy();

	//private final Config config = new ConfigFactory().construct();

	@Value("${local.server.port}")
	int port;

	@Before
	public void setUp() {

		RestAssured.port = port;
	}

	/*
	 * test methods
	 */
	@Ignore
	@Test
	public void addUsers_ReturnsRegistrationId() {
		try{
		given().header("Content-Type", "application/json")
				.body(dummy.getUserRegistrationDto())
				.post("/user-registration/addUser")
				.then().assertThat().statusCode(200);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void getUsers_ReturnsListUsers() {
		try {
			given().get("/user-registration/emailId/" + dummy.getEmail())
			.then()
			.assertThat().statusCode(200);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
