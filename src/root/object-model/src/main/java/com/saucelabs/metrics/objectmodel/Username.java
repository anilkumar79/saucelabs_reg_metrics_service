package com.saucelabs.metrics.objectmodel;

public interface Username {

	String getValue();
}
