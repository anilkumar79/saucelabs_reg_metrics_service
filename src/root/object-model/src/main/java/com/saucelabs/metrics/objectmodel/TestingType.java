package com.saucelabs.metrics.objectmodel;

public interface TestingType {

	String getValue();

}
