package com.saucelabs.metrics.objectmodel;

public class UserRegistrationDtoImpl implements UserRegistrationDto {

    /*
    fields
    */
	private UserRegistrationId userRegistrationId;
	
	private final RegistrationDate registrationDate;
	
	private final TeamName teamName;
	
	private final EmailId emailId;
	
	private final RequestorName requestorName;

    private final TestingType testingType;
    
    private final BitbucketURL bitbucketURL;
    
    private final JenkinsURL jenkinsURL;

    /*
    constructors
    */
    public UserRegistrationDtoImpl(
    		 UserRegistrationId userRegistrationId,
			 RequestorName reqeuestorName,
			 TeamName teamName,
			 EmailId emailId,
			 RegistrationDate registrationDate,
			 TestingType testingType,
			 BitbucketURL bitbucketURL,
			 JenkinsURL jenkinsURL
    ) {

    	this.userRegistrationId = userRegistrationId;
    	
    	this.requestorName = reqeuestorName;
    	
    	this.teamName = teamName;
    	
    	this.emailId = emailId;
    	
    	this.registrationDate = registrationDate;
    			
    	this.testingType = testingType;
    	
    	this.bitbucketURL = bitbucketURL;
    	
    	this.jenkinsURL = jenkinsURL;

    }
    
    /*
    getter & setter methods
    */
	public UserRegistrationId getUserRegistrationId() {
		return userRegistrationId;
	}

	public RegistrationDate getRegistrationDate() {
		return registrationDate;
	}

	public TeamName getLastName() {
		return teamName;
	}

	public EmailId getEmailId() {
		return emailId;
	}

	@Override
	public RequestorName getRequestorName() {
		return requestorName;
	}

	@Override
	public TeamName getTeamName() {
		return teamName;
	}

	@Override
	public TestingType getTestingType() {
		return testingType;
	}

	@Override
	public BitbucketURL getBitbucketURL() {
		return bitbucketURL;
	}

	@Override
	public JenkinsURL getJenkinsURL() {
		return jenkinsURL;
	}

}
