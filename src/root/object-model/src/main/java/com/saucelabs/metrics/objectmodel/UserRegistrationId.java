package com.saucelabs.metrics.objectmodel;

public interface UserRegistrationId {

	public Long getValue();

}
