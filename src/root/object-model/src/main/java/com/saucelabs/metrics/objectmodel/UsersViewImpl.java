package com.saucelabs.metrics.objectmodel;

public class UsersViewImpl implements UsersView {

	/*
    fields
     */

	private final UserRegistrationId userRegistrationId;
	
	private final RequestorName requestorName;
	
	private final TeamName teamName;
	
	private final EmailId emailId;
	
	private final TestingType testingType;
	
	private final RegistrationDate registrationDate;
	
	private final BitbucketURL bitbucketURL;
	
	private final JenkinsURL jenkinsURL;

   	public UsersViewImpl(
    		 UserRegistrationId userRegistrationId,
			 RequestorName requestorName,
			 TeamName lastName,
			 EmailId emailId,
			 RegistrationDate registrationDate,
			 TestingType status,
			 BitbucketURL bitbucketURL,
			 JenkinsURL jenkinsURL
			) {
    	
    	this.userRegistrationId = userRegistrationId;
    	
    	this.requestorName = requestorName;
    	
    	this.teamName = lastName;
    	
    	this.emailId = emailId;
    	
    	this.registrationDate = registrationDate;
    	
    	this.testingType = status;
    	
    	this.bitbucketURL = bitbucketURL;
    	
    	this.jenkinsURL = jenkinsURL;
    			
	}
    
    
    @Override
    public UserRegistrationId getUserRegistrationId() {
		return userRegistrationId;
	}

    @Override
	public RegistrationDate getRegistrationDate() {
		return registrationDate;
	}

    @Override
	public RequestorName getRequestorName() {
		return requestorName;
	}

	@Override
	public TeamName getTeamName() {
		return teamName;
	}

    @Override
	public EmailId getEmailId() {
			return emailId;
	}

	@Override
	public TestingType getTestingType() {
		return testingType;
	}

	@Override
	public BitbucketURL getBitbucketURL() {
		return bitbucketURL;
	}

	@Override
	public JenkinsURL getJenkinsURL() {
		return jenkinsURL;
	}

}
