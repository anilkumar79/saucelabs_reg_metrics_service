package com.saucelabs.metrics.objectmodel;

public interface UsersDto {

	UserRegistrationId getUserRegistrationId();

    RequestorName getFirstName();

    TeamName getSecondName();

    RegistrationDate getRegistrationDate();

    TestingType getStatus();

    BitbucketURL getDomain();

}
