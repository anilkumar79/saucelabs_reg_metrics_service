package com.saucelabs.metrics.objectmodel;

public interface JenkinsURL {

	String getValue();

}
