package com.saucelabs.metrics.objectmodel;

public class UsersDtoImpl implements UsersDto {

    /*
    fields
    */
private final UserRegistrationId userRegistrationId;
	
	private final RegistrationDate registrationDate;
	
	private final TeamName secondName;
	
	private final EmailId emailId;
	
	private final BitbucketURL domain;

    private final RequestorName firstName;

    private final TestingType status;

    /*
    constructors
    */
    public UsersDtoImpl(
    		 UserRegistrationId userRegistrationId,
			 RequestorName firstName,
			 TeamName secondName,
			 EmailId emailId,
			 RegistrationDate registrationDate,
			 TestingType status,
			 BitbucketURL domain
    ) {
    	this.userRegistrationId = userRegistrationId;
    	
    	this.firstName = firstName;
    	
    	this.secondName = secondName;
    	
    	this.emailId = emailId;
    	
    	this.domain = domain;
    	
    	this.registrationDate = registrationDate;
    			
    	this.status = status;

    }
    
    /*
    getter & setter methods
    */
	public UserRegistrationId getUserRegistrationId() {
		return userRegistrationId;
	}

	public RegistrationDate getRegistrationDate() {
		return registrationDate;
	}

	public TeamName getSecondName() {
		return secondName;
	}

	public EmailId getEmailId() {
		return emailId;
	}

	public BitbucketURL getDomain() {
		return domain;
	}

	public RequestorName getFirstName() {
		return firstName;
	}

	public TestingType getStatus() {
		return status;
	}

}
