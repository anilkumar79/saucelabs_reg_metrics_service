package com.saucelabs.metrics.objectmodel;

public interface Password {

	String getValue();
}
