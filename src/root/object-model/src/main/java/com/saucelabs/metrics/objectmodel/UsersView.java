package com.saucelabs.metrics.objectmodel;

public interface UsersView {
	/*
    fields
     */
	  UserRegistrationId getUserRegistrationId();

      RequestorName getRequestorName();

      TeamName getTeamName();
      
      EmailId getEmailId();

      RegistrationDate getRegistrationDate();

      TestingType getTestingType();

      BitbucketURL getBitbucketURL();
      
      JenkinsURL getJenkinsURL();

}
