package com.saucelabs.metrics.objectmodel;

public interface RequestorName {

	String getValue();
}
