package com.saucelabs.metrics.objectmodel;

public interface TeamName {

	String getValue();
}
