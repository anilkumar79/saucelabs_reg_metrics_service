package com.saucelabs.metrics.objectmodel;

public interface BitbucketURL {

	String getValue();

}
