package com.saucelabs.metrics.objectmodel;

public class BitbucketURLImpl implements BitbucketURL {

    /*
    fields
     */
    private final String value;
    
    /*
    constructor methods
     */
    public BitbucketURLImpl(
    	 String value
    		){
    	
    	this.value = value;

    }
    
    /*
    getter methods
     */
	@Override
	public String getValue() {
		
		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BitbucketURLImpl other = (BitbucketURLImpl) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	
}
