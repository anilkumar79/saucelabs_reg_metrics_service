package com.saucelabs.metrics.objectmodel;

import java.sql.Timestamp;

public interface RegistrationDate {

	Timestamp getValue();
	
}
