package com.saucelabs.metrics.objectmodel;

public interface EmailId {

	String getValue();
}
