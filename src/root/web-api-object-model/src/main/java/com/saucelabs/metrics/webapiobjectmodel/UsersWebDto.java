package com.saucelabs.metrics.webapiobjectmodel;


import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;


public class UsersWebDto {

	/*
    fields
     */
	private final Long id;

    private final String requestorName;

    private final String teamName;

    private final String email;

	private final String testingType;
    
    private final String registrationDate;

    private final String bitbucketURL;
    
    private final String jenkinsURL;

    /*
    constructors
     */
    public UsersWebDto()
     {
    	id = 0L;

    	requestorName = null;

    	teamName = null;

    	email = null;
    	
    	registrationDate = null;

    	testingType = null;
    	
    	bitbucketURL = null;

    	jenkinsURL = null;
    }

    public UsersWebDto(
    		@NonNull final Long id,
    		@NonNull final String requestorName,
    		@NonNull final String teamName,
    		@NonNull final String email,
    		@Nullable final String registrationDate,
    		@NonNull final String testingType,
    		@Nullable final String bitbucketURL,
    		@Nullable final String jenkinsURL
    ) {

    	this.id = id;
    	
    	this.requestorName = requestorName;
    	
    	this.teamName = teamName;
    	
    	this.email = email;
    	
    	this.registrationDate = registrationDate;
    	
    	this.testingType = testingType;
    	
    	this.bitbucketURL = bitbucketURL;
    	
    	this.jenkinsURL = jenkinsURL;
    			
    }

    /*
    getter methods
    */
    public Long getId() {
		return id;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public String getTeamName() {
		return teamName;
	}

	public String getEmail() {
		return email;
	}

	public String getTestingType() {
		return testingType;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}

	public String getBitbucketURL() {
		return bitbucketURL;
	}

	public String getJenkinsURL() {
		return jenkinsURL;
	}

}
