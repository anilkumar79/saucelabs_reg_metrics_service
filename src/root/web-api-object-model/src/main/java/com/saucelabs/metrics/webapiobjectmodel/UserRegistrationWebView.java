package com.saucelabs.metrics.webapiobjectmodel;


import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;


public class UserRegistrationWebView {

	/*
    fields
     */
	private final Long id;

    private final String requestorName;

    private final String teamName;

    private final String email;

    private final String testingType;
    
    private final String registrationDate;

    private final String bitbucketURL;
    
    private final String jenkinsURL;

	/*
    constructors
     */
    public UserRegistrationWebView()
     {
    	id = 0L;

    	requestorName = null;

    	teamName = null;

    	email = null;
    	
    	registrationDate = null;

    	testingType = null;
    	
    	bitbucketURL = null;
    	
    	jenkinsURL = null;

    }

    public UserRegistrationWebView(
    		@NonNull final Long id,
    		@NonNull final String firstName,
    		@NonNull final String lastName,
    		@NonNull final String email,
    		@Nullable final String registrationDate,
    		@NonNull final String domain,
    		@Nullable final String bitbucketURL,
    		@Nullable final String jenkinsURL
    ) {

    	this.id = id;
    	
    	this.requestorName = firstName;
    	
    	this.teamName = lastName;
    	
    	this.email = email;
    	
    	this.registrationDate = registrationDate;
    	
    	this.testingType = domain;
    	
    	this.bitbucketURL = bitbucketURL;
    	
    	this.jenkinsURL = jenkinsURL;
    			
    }

    /*
    getter methods
    */
	public Long getId() {
		return id;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public String getTeamName() {
		return teamName;
	}

	public String getEmail() {
		return email;
	}

	public String getTestingType() {
		return testingType;
	}
	
	public String getRegistrationDate() {
		return registrationDate;
	}

	public String getBitbucketURL() {
		return bitbucketURL;
	}
	
	public String getJenkinsURL() {
		return jenkinsURL;
	}

}
