package com.saucelabs.metrics.api;

import com.saucelabs.metrics.database.DatabaseAdapterConfig;


public interface UserRegistrationConfig {

    DatabaseAdapterConfig getDatabaseAdapterConfig();
    
}