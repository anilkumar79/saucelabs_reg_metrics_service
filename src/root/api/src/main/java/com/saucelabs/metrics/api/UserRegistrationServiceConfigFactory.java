package com.saucelabs.metrics.api;

public interface UserRegistrationServiceConfigFactory {

	UserRegistrationConfig construct();
}
