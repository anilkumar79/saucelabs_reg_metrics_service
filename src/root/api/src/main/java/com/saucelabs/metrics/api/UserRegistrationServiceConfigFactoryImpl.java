package com.saucelabs.metrics.api;

import com.saucelabs.metrics.database.DatabaseAdapterConfigFactoryImpl;

public class UserRegistrationServiceConfigFactoryImpl implements
		UserRegistrationServiceConfigFactory {

	@Override
	public UserRegistrationConfig construct() {
		
		return 
				new UserRegistrationServiceConfigImpl(
						new DatabaseAdapterConfigFactoryImpl()
												  .construct()
					);
		
				
	}

}
