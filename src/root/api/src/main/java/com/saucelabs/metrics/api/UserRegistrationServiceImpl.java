package com.saucelabs.metrics.api;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.saucelabs.metrics.core.Core;
import com.saucelabs.metrics.core.CoreImpl;
import com.saucelabs.metrics.database.DatabaseAdapterImpl;
import com.saucelabs.metrics.objectmodel.EmailId;
import com.saucelabs.metrics.objectmodel.UserRegistrationDto;
import com.saucelabs.metrics.objectmodel.UserRegistrationId;
import com.saucelabs.metrics.objectmodel.UsersView;

public class UserRegistrationServiceImpl
		implements UserRegistrationService {

    /*
    fields
     */
    private final Core core;

    /*
    constructors
     */
    public UserRegistrationServiceImpl(
            @NonNull UserRegistrationConfig config
    ) {

        core =
                new CoreImpl(
                        new DatabaseAdapterImpl(
                                config.getDatabaseAdapterConfig()
                        )
                );

    }

	@Override
	public UserRegistrationId addUsers(
			UserRegistrationDto userRegistrationDtos
			){

		return
				core
				.addUsers(
						userRegistrationDtos
						);

	}

	@Override
	public Collection<UsersView> getUsersWithEmailId(
			@NonNull EmailId emailId
			) {
		return
				core
				.getUsersWithIds(
						emailId
						);
	}

}
