package com.saucelabs.metrics.api;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.saucelabs.metrics.objectmodel.EmailId;
import com.saucelabs.metrics.objectmodel.UserRegistrationDto;
import com.saucelabs.metrics.objectmodel.UserRegistrationId;
import com.saucelabs.metrics.objectmodel.UsersView;

public interface UserRegistrationService {

	UserRegistrationId addUsers(
			@NonNull UserRegistrationDto userRegistrationDto
			);

	Collection<UsersView> getUsersWithEmailId(
    		@NonNull EmailId emailId
    		);

}
