package com.saucelabs.metrics.api;


import org.checkerframework.checker.nullness.qual.NonNull;

import com.saucelabs.metrics.database.DatabaseAdapterConfig;


public class UserRegistrationServiceConfigImpl 
		implements UserRegistrationConfig {

    /*
    fields
     */
    private final DatabaseAdapterConfig databaseAdapterConfig;
    

    /*
    constructors
     */
    public UserRegistrationServiceConfigImpl(
            @NonNull final DatabaseAdapterConfig databaseAdapterConfig
    ) {

    	this.databaseAdapterConfig =
                         databaseAdapterConfig;

    }

    /*
    getter methods
    */
    @Override
    public DatabaseAdapterConfig getDatabaseAdapterConfig() {
        return databaseAdapterConfig;
    }

}
